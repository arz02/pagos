<?php
require("../connection.php");
class Model{
	private $tabla;
	
	public static function all(){
		$db = new Connection();
		$res = $db->query('SELECT * FROM '.self::tabla, PDO::FETCH_ASSOC);
		return $res;
	}
	
	public static function save($data, $id = 0){
		$db = new Connection();
		$keys = array_keys($data);
		$values = array_values($data);
		$propiedades = "";
		$valores = array();	
		if($id == 0){			
			$titulos = "";
			for($i = 0; $i <= count($keys); $i++){
				if($i == 0){
					$propiedades .= $keys[$i];
					$titulos .= ":".$keys[$i];
				}
				else{
					$propiedades .= ", ".$keys[$i];
					$titulos .= ", :".$keys[$i];
				}
				$key = ":".$keys[$i];
				$valores[$key]= $values[$i];
			}
			$sql = "INSERT INTO".self::tabla."(".$propiedades.") VALUES (".$titulos.")";
		}
		else{
			for($i = 0; $i <= count($keys); $i++){
				if($i == 0){
					$propiedades .= $keys[$i]."=?";					
				}
				else{
					$propiedades .= ", ".$keys[$i]."=?";
				}				
			}
			$valores=$values;
			array_push($valores, $id);
			$sql = "UPDATE ".self::tabla." SET ".$propiedades.") WHERE id=?";
		}
		try{
			$q = $db->prepare($sql);
			if($q->execute())
			{
				if($id != 0)
				{
					$last = $db->lastInsertId(); 
					return array("ok" => true, "id" => $last);
				}
				else{
					return array("ok" => true, "id" => $id);
				}				
			}	
			else{
				return array("ok" => false, "msg" => "No se pudo insertar el registro");
			}
		}
		catch(Exception $ex){
			return array("ok" => false, "msg" => "Problema al guardar: ".$ex->getMessage());
		}
	}
	
	public static function del($id){
		$db = new Connection();
		$sql = "DELETE FROM ".self::tabla." WHERE id = :id";
		$re = $db->prepare($sql);
		$re->bindParam(':id', $id, PDO::PARAM_INT);   
		return $re->execute();		
	}
	
}
?>